package logic;

import java.awt.BorderLayout;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JFrame;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.CircleOptions;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import data_structures.Graph;
import data_structures.RedBlackBalancedSearchTrees;
import vo.Arcos_Calles;
import vo.InterseccionMallaVial;

/**
 * Clase para crear las instancias del mapa para visualizar el grafo.
 * 
 * @author Geovanny Gonzalez
 */

public class VisualizacionMapa extends MapView {
	// ===========================
	// Atributos
	// ===========================

	// Ventana del Mapa.
	private JFrame ventana;

	// Objeto Mapa de la libreria.
	private Map mapa;

	private static int numeroVentana = 1;

	// ===========================
	// Constructor
	// ===========================

	/**
	 * Permite construir el mapa para agregar los marcadores y visualizarlo en
	 * pantalla.
	 */

//	public VisualizacionMapa(Graph grafo_cargado) {
//		/**
//		 * Creaci�n y configuraci�n del mapa.
//		 */
//
//
//		setOnMapReadyHandler(new MapReadyHandler() {
//			/**
//			 * Configuraci�n de las caracteristicas del mapa.
//			 */
//
//			public void onMapReady(MapStatus estadoMapa) {
//				// Si la creaci�n del mapa ha sido correcta.
//				if (estadoMapa == MapStatus.MAP_STATUS_OK) {
//					// Obtiene el mapa para crear sus caracteristicas.
//					int indiceColor = 0;
//					mapa = getMap();
//					MapOptions opcionesMapa = new MapOptions();
//					opcionesMapa.setMapTypeControlOptions(new MapTypeControlOptions());
//					mapa.setOptions(opcionesMapa);
//
//					// Se centra el mapa en la zona de chicago (zona de interes).
//					mapa.setCenter(new LatLng(38.9041, -77.0171));
//					// Poner zoom de vista
//					mapa.setZoom(12);
//
//					// Agregar vertices.
//
//					Iterator<String> it_vertices = grafo_cargado.darVertices().keys().iterator();
//
//					while (it_vertices.hasNext()) {
//						
//
//						String idActual = it_vertices.next();
//
//						InterseccionMallaVial vertice_actual = grafo_cargado.darVertices().get(idActual);
//
//						Marker marcador_actual = new Marker(mapa);
//
//						LatLng coordenadas_actual = new LatLng(vertice_actual.getLatitud(),
//								vertice_actual.getLongitud());
//
//						marcador_actual.setPosition(coordenadas_actual);
//
//						Iterator<Integer> it2 = grafo_cargado.adj(Integer.parseInt(idActual)).iterator();
//
////						while (it2.hasNext()) {
////
////							String id_vertice_adyacente_actual = it2.next() + "";
////
////							InterseccionMallaVial vertice_adyacente_actual = grafo_cargado.darVertices()
////									.get(id_vertice_adyacente_actual);
////
////							PolylineOptions detallesArco = new PolylineOptions(); // Configura el grosor de la linea
////							detallesArco.setStrokeOpacity(1.75);
////							detallesArco.setStrokeWeight(1.5);
////
////							// Coordenadas de la nueva linea (arco)
////							LatLng[] coordenadas_arco = {
////									new LatLng(vertice_actual.getLatitud(), vertice_actual.getLongitud()),
////									new LatLng(vertice_adyacente_actual.getLatitud(),
////											vertice_adyacente_actual.getLongitud()) }; // Coordenadas de los vertices
////																						// inicio y fin.
////
////							Polyline nuevo_arco = new Polyline(mapa); // Configura la linea en el mapa
////							nuevo_arco.setPath(coordenadas_arco); // Configura las coordenadas
////							detallesArco.setStrokeColor("#DF0101"); // Da algo de color
////							nuevo_arco.setOptions(detallesArco); // Configura la caracteristicas de la instancia de la
////																	// linea // linea
////
////						}
//
//					}
//					
//					System.out.println("termina");
//				}
//			}
//		});
//	}
	
	public VisualizacionMapa(LinkedList<InterseccionMallaVial> camino) {
		/**
		 * Creaci�n y configuraci�n del mapa.
		 */


		setOnMapReadyHandler(new MapReadyHandler() {
			/**
			 * Configuraci�n de las caracteristicas del mapa.
			 */

			public void onMapReady(MapStatus estadoMapa) {
				
				int contador = 0;
				// Si la creaci�n del mapa ha sido correcta.
				if (estadoMapa == MapStatus.MAP_STATUS_OK) {
					// Obtiene el mapa para crear sus caracteristicas.
					int indiceColor = 0;
					mapa = getMap();
					MapOptions opcionesMapa = new MapOptions();
					opcionesMapa.setMapTypeControlOptions(new MapTypeControlOptions());
					mapa.setOptions(opcionesMapa);

					// Se centra el mapa en la zona de chicago (zona de interes).
					mapa.setCenter(new LatLng(38.9041, -77.0171));
					// Poner zoom de vista
					mapa.setZoom(12);

					// Agregar vertices.
					
					for (int i = 0; i < camino.size(); i++) {
						
						InterseccionMallaVial vertice_actual = camino.get(i);
						
					
						Marker marcador_actual = new Marker(mapa);

						LatLng coordenadas_actual = new LatLng(vertice_actual.getLatitud(),
								vertice_actual.getLongitud());
						
						marcador_actual.setPosition(coordenadas_actual);
						
					}
					
					for (int i = 0; (i+1) < camino.size(); i++) {
						
						InterseccionMallaVial vertice_actual = camino.get(i);
						
					for (int j = i+1; j < camino.size(); j++) {
						
						InterseccionMallaVial vetice_siguiente = camino.get(j);
						
						PolylineOptions detallesArco = new PolylineOptions(); // Configura el grosor de la linea
						detallesArco.setStrokeOpacity(1.75);
						detallesArco.setStrokeWeight(1.5);

						// Coordenadas de la nueva linea (arco)
						LatLng[] coordenadas_arco = {
								new LatLng(vertice_actual.getLatitud(), vertice_actual.getLongitud()),
								new LatLng(vetice_siguiente.getLatitud(),
										vetice_siguiente.getLongitud()) }; // Coordenadas de los vertices
																					// inicio y fin.

						Polyline nuevo_arco = new Polyline(mapa); // Configura la linea en el mapa
						nuevo_arco.setPath(coordenadas_arco); // Configura las coordenadas
						detallesArco.setStrokeColor("#DF0101"); // Da algo de color
						nuevo_arco.setOptions(detallesArco); // Configura la caracteristicas de la instancia de la
																// linea				
						}
					}
				}	
			}
		}
	);

	}

	/**
	 * Permite visualizar el mapa en la pantalla. <b>post:</b> Se visualiza el mapa
	 * desde el proyecto.
	 */

	public void visualizarMapa() {

		try {
			// Instancia de una ventana JFrame
			ventana = new JFrame("Mapa proyecto 3 - Ventana # " + numeroVentana);
			numeroVentana++; // Incrementar el numero de ventanas.
			ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			ventana.setResizable(true);
			ventana.setSize(860, 432);
			ventana.add(this, BorderLayout.CENTER);
			ventana.setVisible(true);
		} catch (Exception e) {
			System.out.println(".");
		}
	}

}
