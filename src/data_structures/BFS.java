package data_structures;

import java.util.HashMap;
import java.util.LinkedList;

import vo.InterseccionMallaVial;

public class BFS {



	public LinkedList<InterseccionMallaVial> bfs(InterseccionMallaVial vInicial, Graph grafo){
		HashMap<Long, Integer> marcados = new HashMap<>();
		InterseccionMallaVial actual = vInicial;
		LinkedList<InterseccionMallaVial> bfs = new LinkedList<>();

		LinkedList<InterseccionMallaVial> cola = new LinkedList<>();
		bfs.add(actual);
		cola.add(actual);
		while(!cola.isEmpty() && actual != null){
			for(String v : actual.darAdyacentes()){
				Long adyacente = Long.parseLong(v);
				InterseccionMallaVial adj = grafo.darVertices().get(adyacente+"");
				if(!marcados.containsKey(adj.objectId())){
					cola.add(adj);
					marcados.put(Long.parseLong(adj.objectId()), 1);
					bfs.add(adj);
				}
			}
			actual = cola.poll();
		}
		return bfs;
	}

	/**
	 * Encuentra el menor camino en terminos de numero de vertices recorridos entre dos vertices
	 * @return camino encontrado
	 */
	public LinkedList<InterseccionMallaVial> menorCaminoVertices(InterseccionMallaVial v1, InterseccionMallaVial v2, Graph grafo){

		if(v1 == null || v2 == null || grafo == null){
			throw new IllegalArgumentException();
		}

		HashMap<Long, Integer> marcados = new HashMap<>();
		HashMap<Long, Integer> distancias = new HashMap<>();
		InterseccionMallaVial actual = v1;

		LinkedList<InterseccionMallaVial> bfs = new LinkedList<>();

		LinkedList<InterseccionMallaVial> cola = new LinkedList<>();
		bfs.add(v1);
		cola.add(v1);
		while(actual != v2 && actual != null) {
			for(String v : actual.darAdyacentes()) {
				Long adyacente = Long.parseLong(v);
				InterseccionMallaVial adj = grafo.darVertices().get(adyacente+"");
				if (!marcados.containsKey(adj.objectId())) {
					cola.add(adj);
					marcados.put(Long.parseLong(adj.objectId()), 1);
					bfs.add(adj);
				}
			}
			actual = cola.poll();
		}


		return cola;
	}
	
	
	
	
	//2
	
	  HashMap<Long, Integer> marcados;

	  public LinkedList<InterseccionMallaVial> bfs2(InterseccionMallaVial vInicial, Graph grafo) {
	    marcados = new HashMap<>();
	    InterseccionMallaVial actual = vInicial;
	    LinkedList<InterseccionMallaVial> bfs = new LinkedList<>();

	    LinkedList<InterseccionMallaVial> cola = new LinkedList<>();
	    bfs.add(actual);
	    cola.add(actual);
	    while (!cola.isEmpty()) {
	      actual = cola.poll();
	      for (String v : actual.darAdyacentes()) {
	        Long adyacente = Long.parseLong(v);
	        InterseccionMallaVial adj = grafo.darVertices().get((adyacente+""));
	        if (!marcados.containsKey(adj.objectId())) {
	          cola.add(adj);
	          marcados.put(Long.parseLong(adj.objectId()), 1);
	          bfs.add(adj);
	        }
	      }
	    }
	    return bfs;
	  }

	  /**
	   * Devuelve un mapeo que contiene las distancias de todos los vertices entre el vertice inicial y
	   * el vertice final
	   */
	  private HashMap<Long, Integer> menorCaminoVertices2(InterseccionMallaVial v1, InterseccionMallaVial v2, Graph grafo) {

	    if (v1 == null || v2 == null || grafo == null) {
	      throw new IllegalArgumentException();
	    }

	    marcados = new HashMap<>();
	    HashMap<Long, Integer> distancias = new HashMap<>();
	    //Mapa que contiene los predecesores de cada nodo
	    InterseccionMallaVial actual = v1;

	    LinkedList<InterseccionMallaVial> cola = new LinkedList<>();
	    cola.add(v1);
	    distancias.put(Long.parseLong(actual.objectId()), 0);
	    int counter = 0;
	    while (actual != v2 && !cola.isEmpty()) {
	      actual = cola.poll();
	      for (String v : actual.darAdyacentes()) {
	        Long adyacente = Long.parseLong(v);
	        InterseccionMallaVial adj = grafo.darVertices().get(adyacente+"");
	        if (!marcados.containsKey(adj.objectId())) {
	          cola.add(adj);
	          marcados.put(Long.parseLong(adj.objectId()), 1);
	          distancias.put(adyacente, distancias.get(actual.objectId()) + 1);
	        }
	        if(adj == v2){
	          actual = adj;
	          break;
	        }
	      }
	      counter++;
	    }
	    return distancias;
	  }

	  /**
	   * Devuelve el menor camino entre dos vertices dado un mapeo de las distancias entre dos caminos
	   */
	  public LinkedList<InterseccionMallaVial> menorCamino(InterseccionMallaVial v1, InterseccionMallaVial v2, Graph grafo) {
	    HashMap<Long, Integer> distancias = menorCaminoVertices2(v1, v2, grafo);
	    marcados = new HashMap<>();
	    //Mapa que contiene los predecesores de cada nodo
	    InterseccionMallaVial actual = v2;
	    LinkedList<InterseccionMallaVial> retorno = new LinkedList<>();
	    InterseccionMallaVial verticeMenor;

	    while (actual != v1) {
	      int distanciaMenor = Integer.MAX_VALUE;
	      verticeMenor = actual;
	      for (String v : actual.darAdyacentes()) {
	        Long adyacente = Long.parseLong(v);
	        if (distancias.containsKey(adyacente) && !marcados.containsKey(adyacente)) {
	          int distancia = distancias.get(adyacente);
	          marcados.put(adyacente, 1);
	          if (distancia <= distanciaMenor) {
	            distanciaMenor = distancia;
	            verticeMenor = grafo.darVertices().get(adyacente+"");
	          }
	        }
	        if(verticeMenor == v1){
	          break;
	        }
	      }
	      actual = verticeMenor;
	      retorno.add(verticeMenor);
	    }
	    Stack<InterseccionMallaVial> stack = new Stack<>();
	    for(InterseccionMallaVial v : retorno){
	      stack.push(v);
	    }
	    retorno = new LinkedList<>();
	    while(!stack.isEmpty()){
	      retorno.add(stack.pop());
	    }
	    retorno.add(v2);
	    return retorno;
	  }

}