package vo;

import java.util.LinkedList;

public class arco_final {
	/**
	 * representa el ID 
	 */
	private String objectID;
	private InterseccionMallaVial vertice1;
	private InterseccionMallaVial vertice2;
	private int peso_suma_infracciones;
	private Double distancia_entre_vertices;


	

	public arco_final (InterseccionMallaVial verticePrimero, InterseccionMallaVial verticeSegundo) 
	{
		
		vertice1 = verticePrimero;
		vertice2 = verticeSegundo;
		peso_suma_infracciones = verticePrimero.getCantidadInfracciones() + verticeSegundo.getCantidadInfracciones();
		distancia_entre_vertices = CalcularDistanciaHarversiana(verticePrimero.getLatitud(),verticePrimero.getLongitud(), verticeSegundo.getLatitud(), verticeSegundo.getLongitud());
		objectID =generarId()+"";

	}
	
	public double CalcularDistanciaHarversiana(double lat1, double lng1, double lat2, double lng2) {
		// double radioTierra = 3958.75;//en millas
		double radioTierra = 6371;// en kilómetros
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double va1 = Math.pow(sindLat, 2)
				+ Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
		double distancia = radioTierra * va2;

		return distancia;
	
	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public String getIdentificador() 
	{
		return objectID;
	}
	public InterseccionMallaVial darVertice1() 
	{
		return vertice1;
	}
	public InterseccionMallaVial darVertice2() 
	{
		return vertice2;
	}
	
	public Double darDistanciaEntreVertices(){
		return distancia_entre_vertices;
	}
	
	public int getCantidadInfracciones(){
		return peso_suma_infracciones;
	}
	
	public int generarId(){
	long id1 = Long.parseLong(vertice1.objectId());
	long id2 = Long.parseLong(vertice2.objectId());
	
	id1 = (long) (id1 + vertice2.getLatitud());
	id2 =  (long) (id2 + vertice2.getLongitud());
	
	id1 = (long) (id1 + vertice1.getLatitud());
	id2 = (long) (id2 + vertice2.getLongitud());
	double suma = id1+id2;
	suma += distancia_entre_vertices;
	suma = suma *21;
	String  code = suma + "";
	
	return code.hashCode();
	}

	public void setId(String objectId2) {
		this.objectID = objectId2;
		
	}
	
	public boolean contieneVertice(InterseccionMallaVial vertice) 
	{
		boolean RTA = false;
		
		if(vertice1.objectId()==vertice.objectId() && vertice2.objectId()==vertice.objectId()) {
			RTA = true;
		}
		return RTA;
	}

	public void setVertices(LinkedList<String> darVertices) {
	
	}
}