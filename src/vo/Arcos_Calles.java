package vo;

import java.util.ArrayList;
import java.util.LinkedList;

import data_structures.IQueue;
import data_structures.Queue;

public class Arcos_Calles{
	/**
	 * representa el ID 
	 */
	private String objectID;	
	private LinkedList<String>vertices = new LinkedList<>();

	

	public Arcos_Calles (String pObjectID,LinkedList<String> verticesActual) 
	{
		objectID =pObjectID;
		vertices = verticesActual;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public String objectId() 
	{
		return objectID;
	}
	public LinkedList<String> darVertices() 
	{
		return vertices;
	}
	
	public void setId(String pId)
	{
		objectID = pId;
	}
	
	public void setVertices(LinkedList<String> nuevosVertices)
	{
		vertices = nuevosVertices;
	}
	
	
	public String toString() {
		return ("Identificador : " + objectID + "; vertices : " + vertices.toString().replace(",", "-"));
	}
	
}
