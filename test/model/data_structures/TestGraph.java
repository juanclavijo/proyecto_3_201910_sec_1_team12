package model.data_structures;

import java.util.ArrayList;
import java.util.LinkedList;

import data_structures.Graph;
import data_structures.IQueue;
import data_structures.Queue;
import junit.framework.TestCase;
import vo.Arcos_Calles;
import vo.InterseccionMallaVial;

public class TestGraph extends TestCase
{
	private Graph grafo = new Graph();
	
	public void setUpExcenario1()
	{
		InterseccionMallaVial i1 = new InterseccionMallaVial("12345", "234314.0", "45234.0");
		InterseccionMallaVial i2 = new InterseccionMallaVial("124", "23434514.0", "3455234.0");
		InterseccionMallaVial i3 = new InterseccionMallaVial("64354", "25436314.0", "-45234.0");
		InterseccionMallaVial i4 = new InterseccionMallaVial("2345", "64514.0", "345234.0");
		InterseccionMallaVial i5 = new InterseccionMallaVial("16356", "756314.0", "645234.0");
		
		LinkedList<String> array = new LinkedList<String>();
		array.add("3243");
		array.add("3423413");
		Arcos_Calles a1 = new Arcos_Calles("354", array);


		grafo.addVertex("12345", i1);
		grafo.addVertex("124", i2);
		grafo.addVertex("64354", i3);
		grafo.addVertex("2345", i4);
		grafo.addVertex("16356", i5);
		
	//	grafo.addEdge2("12345", "123", a1);
		
	}
	
	
	public void testV()
	{
		setUpExcenario1();
		
		assertEquals("debe ser 5", 5, grafo.V());
	}
	
	public void testE()
	{
		setUpExcenario1();
		
		assertEquals("debe ser 1", 1, grafo.E());
	}
	
	public void testGetInfoVertex()
	{
		setUpExcenario1();
		InterseccionMallaVial r = grafo.getInfoVertex("12345");
		assertEquals("debe ser 234314.0", 234314.0, r.getLatitud());
	}
	
	public void setInfoVertex()
	{
		setUpExcenario1();
		InterseccionMallaVial r = new InterseccionMallaVial("4", "3.0", "1.0");
		grafo.setInfoVertex("12345", r);
		InterseccionMallaVial nuevo = grafo.getInfoVertex("4");
		assertNotNull("no deberia ser null", nuevo);
	}
	
//	public void getInfoArc()
//	{
//		setUpExcenario1();
//		Arcos_Calles r = grafo.getInfoArc("12345","124");
//		assertEquals("debe ser 3423413", 3423413, r.darVertices().get(1));
//
//	}
//	
//	public void setInfoArc()
//	{
//		setUpExcenario1();
//		LinkedList<String> array = new LinkedList<String>();
//		array.add("3");
//		array.add("34");
//		Arcos_Calles a1 = new Arcos_Calles("354", array);
//		grafo.setInfoArc("12345", "123", a1);
//		Arcos_Calles r = grafo.getInfoArc("12345", "123");
//		assertEquals("debe ser 3", 3, r.darVertices().get(0));
//
//	}

}