package model.data_structures;



import data_structures.Queue;
import junit.framework.TestCase;

public class TestQueue extends TestCase {

	Queue<Integer> arreglo = new Queue<Integer>();

	/**
	 * Arreglo con los elementos del escenario
	 */
	protected static final int[] ARREGLO_ESCENARIO = { 350, 383, 105, 233, 140, 266, 356, 236, 80, 360, 221, 241, 130,
			244, 352, 446, 18, 98, 97, 396 };

	public void setupEscenario1() {
		for (int actual : ARREGLO_ESCENARIO) {
			arreglo.enqueue(actual);
		}

	}

	public void testenqueue() throws Exception {
		// Prueba la lista vac�a.
		assertTrue("La lista debe estar vacia", arreglo.isEmpty());
		assertEquals("no es 0", 0, arreglo.size());

		// Agrega dos elementos.

		arreglo.enqueue(5);
		arreglo.enqueue(10);
		assertFalse("La lista no deberia estar vacia", arreglo.isEmpty());
		assertEquals("Debe contener 2 elementos", 2, arreglo.size());
		assertFalse("La lista no contiene 5", arreglo.size() == (5));
	}

	public void isEmpty() {

		assertEquals(true, arreglo.isEmpty());
		// Se agregan elementos
		setupEscenario1();
		assertEquals(false, arreglo.isEmpty());

	}


	public void testGetSize() {
		// Prueba la lista vac�a.
		assertEquals("El tamaño de la lista vacia no es correcto", 0, arreglo.size());

		// Prueba la lista con dos elementos

		arreglo.enqueue(5);
		arreglo.enqueue(30);

		assertEquals("El tama�o de la lista con dos elementos no es correcto", 2, arreglo.size());
		
		arreglo.dequeue();
		arreglo.dequeue();

		// Prueba la lista con 20 elementos
		setupEscenario1();
		assertEquals("El tama�o de la lista con 20 elementos no es correcto", ARREGLO_ESCENARIO.length, arreglo.size());

	}
}
